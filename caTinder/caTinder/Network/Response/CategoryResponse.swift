//
//  CategoryResponse.swift
//  caTinder
//
//  Created by MACsimus on 21.05.2020.
//  Copyright © 2020 VladyslavSavonik. All rights reserved.
//

import Foundation

struct CategoryResponse : Codable {
    
    let id: Int
    let name: String
}
