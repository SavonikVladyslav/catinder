//
//  RateCateVc.swift
//  caTinder
//
//  Created by MACsimus on 11.05.2020.
//  Copyright © 2020 VladyslavSavonik. All rights reserved.
//

import UIKit

extension MainScreenVC {
    
    func toggleMenu() {                     
        if darkFillView.transform == .identity {
            UIView.animate(withDuration: 1, animations: {
                self.darkFillView.transform = CGAffineTransform(scaleX: 11, y: 11)
                self.menuView.transform = CGAffineTransform(translationX: 0, y: -67)
                self.toggleMenuButton.transform = CGAffineTransform(rotationAngle: self.radians(degreese: 180))
            }) { (true) in
                UIView.animate(withDuration: 0.5, animations: {
                    self.toggleButtons()
                })
            }
        } else  {
            UIView.animate(withDuration: 1, animations: {
                self.darkFillView.transform = .identity
                self.menuView.transform = .identity
                self.toggleMenuButton.transform = .identity
                self.toggleButtons()
            })
        }
    }
    func hideButtons() {
        dislikeButton.alpha = 0
        likeButton.alpha = 0
        favouritesButton.alpha = 0
        settingsButton.alpha = 0
    }
    func radians (degreese: Double) -> CGFloat {
        return CGFloat(degreese * .pi/degreese)
    }
    func toggleButtons() {
        let alpha = CGFloat(dislikeButton.alpha == 0 ? 1 : 0)          //check if buttons hide or no & and turn them in opposite value
        dislikeButton.alpha = alpha
        likeButton.alpha = alpha
        favouritesButton.alpha = alpha
        settingsButton.alpha = alpha
    }
}
