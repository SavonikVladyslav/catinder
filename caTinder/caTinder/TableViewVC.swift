//
//  TableView.swift
//  caTinder
//
//  Created by MACsimus on 22.05.2020.
//  Copyright © 2020 VladyslavSavonik. All rights reserved.
//

import UIKit
import CoreData

class TableViewVC: UITableViewController {
    
    private var sourseArray: [CatCoreData] = []
    private let context = CoreDataManager.persistentContainer.viewContext
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reloadSourseArray()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sourseArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomTableViewCell
        let cat = sourseArray[indexPath.row]
        
        cell.nameLabel?.text = cat.name
        cell.breedLabel.text = cat.breed
        cell.categoryLabel.text = cat.category
        
        cell.imageOfCat.image = UIImage(data: cat.image!)
        cell.imageOfCat?.layer.cornerRadius = cell.imageOfCat.frame.size.height / 2
        cell.imageOfCat?.clipsToBounds = true
        return cell
        
    }
    
    @IBAction func unwindSegue(_ segue: UIStoryboardSegue) {
        guard let NewCatVC = segue.source as?  NewCatViewController else { return }
        NewCatVC.saveNewCat()
        
        reloadSourseArray()
    }
    
    @IBAction func backToMainPage(_ sender: Any) {
        dismiss(animated: true)
    }
    private func reloadSourseArray() {
        sourseArray = CoreDataManager.allEntry()
        tableView.reloadData()
    }
    private func deleteItem(_ cat: CatCoreData) {
        CoreDataManager.removeEntry(cat)
        CoreDataManager.saveContext()
        reloadSourseArray()
    }
}


extension TableViewVC {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteItem(sourseArray[indexPath.row])
        }
    }
}
