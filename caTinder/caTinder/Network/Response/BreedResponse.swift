//
//  setPhotoResponse.swift
//  caTinder
//
//  Created by MACsimus on 21.05.2020.
//  Copyright © 2020 VladyslavSavonik. All rights reserved.
//

import Foundation

struct BreedResponse: Codable {
    
    let id: String
    let name: String
    
}
