//
//  CustomTableViewCell.swift
//  caTinder
//
//  Created by MACsimus on 22.05.2020.
//  Copyright © 2020 VladyslavSavonik. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var imageOfCat: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var breedLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
}
