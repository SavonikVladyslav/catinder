//
//  NetworkMethod.swift
//  caTinder
//
//  Created by MACsimus on 12.05.2020.
//  Copyright © 2020 VladyslavSavonik. All rights reserved.
//

import Foundation

enum HTTPMethod: String {

    case get =  "GET"
    case post = "POST"
    case delete = "DELETE"

}



