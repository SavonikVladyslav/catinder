//
//  FindYourCatVC.swift
//  caTinder
//
//  Created by MACsimus on 11.05.2020.
//  Copyright © 2020 VladyslavSavonik. All rights reserved.
//

import UIKit

struct settingsInfoStruct {
    
    var category: String?
    var idCategory: Int?
    var breed: String?
    var idBreed: String?
}



class MainScreenVC: UIViewController {
    
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var darkFillView: UIView!
    @IBOutlet weak var toggleMenuButton: UIButton!
    
    @IBOutlet weak var dislikeButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var favouritesButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    
    @IBOutlet weak var catBreed: UILabel!
    @IBOutlet weak var catCategory: UILabel!
    
    @IBOutlet weak var card: UIView!
    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var catPicture: UIImageView!
    
    var divisor: CGFloat!
    var photos = [String]()
    var categoryId: Int?
    var breedId: String?
    var onClose: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        divisor  = (view.frame.width / 2) / 0.61
        hideButtons()
        loadPicture()
    }
    
    @IBAction func showSettngsVc(_ sender: UIButton) {
        let view = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVcID") as! SettingsVC
        view.onClose = { settings in
            self.catCategory.text = settings.category
            self.categoryId = settings.idCategory
            self.catBreed.text = settings.breed
            self.breedId = settings.idBreed
            
            self.loadPicture()
        }
        view.modalPresentationStyle = .fullScreen
        show(view, sender: self)
    }
    
    @IBAction func toggleMenu(_ sender: UIButton) {
        toggleMenu()
    }
    //MARK:- card swiping
    @IBAction func panCard(_ sender: UIPanGestureRecognizer) {
        let card = sender.view!
        let point = sender.translation(in: view)
        let xFromCenter = card.center.x - view.center.x     // right + or left - from the centr
        let scale =  min(100/abs(xFromCenter), 1)
        
        // card rotation   100/2 = 50/0.61 = 81.967
        
        card.center = CGPoint(x: view.center.x +  point.x, y: view.center.y + point.y)
        card.transform = CGAffineTransform(rotationAngle: xFromCenter / divisor).scaledBy(x: scale, y: scale)
        
        if xFromCenter > 0 {
            thumbImageView.image = UIImage(named: "ThumbsUp")
            thumbImageView.tintColor = UIColor.green
        } else {
            thumbImageView.image = UIImage(named: "ThumbsDown")
            thumbImageView.tintColor = UIColor.red
        }
        thumbImageView.alpha = abs(xFromCenter / view.center.x)
        if sender.state == UIGestureRecognizer.State.ended {
            if card.center.x < 75 {
                //move to the left side
                
                UIView.animate(withDuration: 0.3, animations: {
                    card.center = CGPoint(x:  card.center.x - 200, y: card.center.y + 75)
                    card.alpha = 0
                })
                
                loadPicture()
                resetCard()
                
                return
            } else if card.center.x > (view.frame.width - 75) {
                //move to the  right side
                UIView.animate(withDuration: 0.3, animations: {
                    card.center = CGPoint(x:  card.center.x + 200, y: card.center.y + 75)
                    card.alpha = 0
                })
                
                loadPicture()
                resetCard()
                
                return
            }
            //returning card to norm possition
            UIView.animate(withDuration: 0.2, animations: {
                self.card.center = self.view.center                             // change not to center
                self.thumbImageView.alpha = 0
                self.card.alpha = 1
                self.card.transform  = .identity
            })
            
        }
    }
    
    @IBAction func dislikeButton(_ sender: UIButton) {
        thumbImageView.alpha = 1
        thumbImageView.image = UIImage(named: "ThumbsDown")
        thumbImageView.tintColor = UIColor.red
        
        loadPicture()
        resetCard()
    }
    
    @IBAction func likeButton(_ sender: UIButton) {
        
        thumbImageView.alpha = 1
        thumbImageView.image = UIImage(named: "ThumbsUp")
        thumbImageView.tintColor = UIColor.green
        
        let newCat: CatCoreData? = CoreDataManager.newEntity()
        newCat?.name = "Liked Photo"
        newCat?.breed = self.catBreed.text
        newCat?.category = self.catCategory.text
        newCat?.image = self.catPicture.image?.pngData()
        CoreDataManager.saveContext()
        
        loadPicture()
        resetCard()
    }
    
    func loadPicture () {
        var urlParams: [URLQueryItem] = [URLQueryItem(name: "limit", value: "1")]
        
        if categoryId != nil {
            urlParams.append(URLQueryItem(name: "category_ids", value: "\(categoryId!)"))
        }
        if breedId != nil {
            urlParams.append(URLQueryItem(name: "breed_id", value: "\(breedId!)"))
        }
        
        NetworkManager.sendRequest(PhotoRequest( { (result) in
            let imageTask = URLSession.shared.dataTask(with: URL(string: result[0].url)!) { (imageData, _, imageError) in
                if let imageError = imageError { print(imageError); return }
                DispatchQueue.main.async {
                    self.catPicture.image = UIImage(data: imageData!)
                }
            }
            imageTask.resume()
        }, { (err) in
            debugPrint("EBY etot Eror")
        }), urlParams)
    }
    
    func resetCard() {
        UIView.animate(withDuration: 0.2, animations: {
            self.card.center = self.view.center
            self.thumbImageView.alpha = 0
            self.card.alpha = 1
            self.card.transform  = .identity
        })
    }
}
//MARK:- end reset func



