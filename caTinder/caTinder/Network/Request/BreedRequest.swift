//
//  setPicture.swift
//  caTinder
//
//  Created by MACsimus on 21.05.2020.
//  Copyright © 2020 VladyslavSavonik. All rights reserved.
//

import Foundation

class BreedRequest: PRequest {
    
    var currentPath: String = "https://api.thecatapi.com/v1/breeds"
    
    var method: HTTPMethod = .get
    var header: HTTPHeaders? = ["X-Api-Key": "df5878ac-e934-4751-bdf1-e670b37a6ab6"]
    var body: HTTPBody?
    
    var onSucces: ((Data) -> Void)
    var onError: ((Error?) -> Void)
    
    
    init(_ onComplite: @escaping (([BreedResponse]) -> Void),_ onError: @escaping ((Error?) -> Void)) {
        
        currentPath = String(format: currentPath)
        self.onError = onError
        self.onSucces = { data in
            
            guard let decodedObject = try? JSONDecoder().decode([BreedResponse].self, from: data) else { onError(nil); return }
            onComplite(decodedObject)
        }
    }
}
