//
//  NetworkManager.swift
//  caTinder
//
//  Created by MACsimus on 12.05.2020.
//  Copyright © 2020 VladyslavSavonik. All rights reserved.
//

import Foundation

public typealias HTTPHeaders = [String: String]
public typealias HTTPBody = [String: Any]

protocol PRequest {
    
    var currentPath: String { get }
    var method: HTTPMethod { get }
    var header: HTTPHeaders? { get }
    var body: HTTPBody? { get }
    
    var onSucces: ((Data) -> Void) { get }
    var onError: ((Error?)-> Void) { get }

}

class NetworkManager {
    class func sendRequest (_ request: PRequest, _ requestParams: [URLQueryItem]) {
        
        guard var currentUrl = URLComponents(string: request.currentPath) else {return}
        
        currentUrl.queryItems = requestParams
        
        var currentRequest = URLRequest(url: currentUrl.url!)
        currentRequest.httpMethod = request.method.rawValue
        currentRequest.allHTTPHeaderFields = request.header
        
        
        if let body = request.body, let jsonData = try? JSONSerialization.data(withJSONObject: body, options: .prettyPrinted) {
            currentRequest.httpBody = jsonData
        }
        URLSession.shared.dataTask(with: currentRequest) { (data, response, error) in
            if let currentError = error { request.onError(currentError); return }
            if let currentData = data {
//                debugPrint(request)
//                debugPrint("============================")
//                debugPrint(request.header)
                if let currentJson = try? JSONSerialization.jsonObject(with: data!, options: [] ) { debugPrint(currentJson)}
                request.onSucces(currentData)
            }
        }.resume()
    }
}

