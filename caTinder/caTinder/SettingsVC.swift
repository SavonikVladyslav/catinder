//
//  SettingsVC.swift
//  caTinder
//
//  Created by MACsimus on 18.05.2020.
//  Copyright © 2020 VladyslavSavonik. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {
    
    @IBOutlet weak var selectACategoryButton: UIButton!
    @IBOutlet var categories: [UIButton]!
    @IBOutlet weak var selectABreedButton: UIButton!
    @IBOutlet var breeds: [UIButton]!
    
    @IBOutlet weak var categorySwitch: UISwitch!
    @IBOutlet weak var breedSwitch: UISwitch!
    
    var onClose: ((settingsInfoStruct) -> Void)?
    var settingsInfo = settingsInfoStruct()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NetworkManager.sendRequest(CategoryRequest( { (result) in
            DispatchQueue.main.async {
                for index in 0..<self.categories.count {
                    self.categories[index].setTitle(result[index].name, for: .normal)
                    self.categories[index].tag = result[index].id
                }
            }
        }, { (err) in
            debugPrint("Error in network response")
        }), [URLQueryItem(name: "limit", value: "\(categories.count)")])
        
        NetworkManager.sendRequest(BreedRequest( { (result) in
            DispatchQueue.main.async {
                for index in 0..<self.breeds.count {
                    
                    self.breeds[index].setTitle(result[index].name, for: .normal)
                    self.breeds[index].accessibilityIdentifier = result[index].id
                }
            }
        }, { (err) in
            debugPrint("Error in network response")
        }), [URLQueryItem(name: "limit", value: "\(breeds.count)")])
        
    }
    
    @IBAction func toggleCategorySwitch(_ sender: UISwitch) {
        selectACategoryButton.isEnabled = sender.isOn
        categories.forEach({$0.isEnabled = sender.isOn})
        
        if sender.isOn == true {
            breedSwitch.isOn = false
            selectABreedButton.isEnabled = false
            breeds.forEach({$0.isEnabled = false})
            breeds.forEach({$0.setTitleColor(.darkGray, for: .normal)})
            selectABreedButton.setTitle("Select a breed", for: .normal)
            
            categories.forEach({$0.setTitleColor(.systemGreen, for: .normal)})
        }
        else {
            categories.forEach({$0.setTitleColor(.darkGray, for: .normal)})
            selectACategoryButton.setTitle("Select a category", for: .normal)
        }
    }
    
    @IBAction func toggleBreedSwitch(_ sender: UISwitch) {
        selectABreedButton.isEnabled = sender.isOn
        breeds.forEach({$0.isEnabled = sender.isOn})
        if sender.isOn == true {
            categorySwitch.isOn = false
            selectACategoryButton.isEnabled = false
            categories.forEach({$0.isEnabled = false})
            categories.forEach({$0.setTitleColor(.darkGray, for: .normal)})
            selectACategoryButton.setTitle("Select a breed", for: .normal)
            
            breeds.forEach({$0.setTitleColor(.systemGreen, for: .normal)})
        }
        else {
            breeds.forEach({$0.setTitleColor(.darkGray, for: .normal)})
            selectABreedButton.setTitle("Select a category", for: .normal)
        }
    }
    
    @IBAction func breedsTapped(_ sender: UIButton) {
        
        guard let titleBreed = sender.currentTitle else {
            return
        }
        selectABreedButton.setTitle(titleBreed, for: .normal)
        selectABreedButton.accessibilityIdentifier = sender.accessibilityIdentifier
    }
    
    @IBAction func categoriesTapped(_ sender: UIButton) {
        
        guard let titleCategory = sender.currentTitle else {
            return
        }
        selectACategoryButton.setTitle(titleCategory, for: .normal)
        selectACategoryButton.tag = sender.tag
    }
    
    @IBAction func backFromSettings(_ sender: UIButton) {
        if categorySwitch.isOn {
            settingsInfo.category = "\(selectACategoryButton.titleLabel?.text ?? "Choosen Categories")"
            settingsInfo.idCategory = selectACategoryButton.tag
        }
        else if breedSwitch.isOn {
            settingsInfo.breed = "\(selectABreedButton.titleLabel?.text ?? "Choosen Breed")"
            settingsInfo.idBreed = selectABreedButton.accessibilityIdentifier
        }
        onClose!(settingsInfo)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
