//
//  ViewController.swift
//  caTinder
//
//  Created by MACsimus on 09.05.2020.
//  Copyright © 2020 VladyslavSavonik. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var findLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        bgImage.alpha = 0
        titleLabel.alpha = 0
        descLabel.alpha = 0
        playButton.alpha = 0
        findLabel.alpha = 0
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.5, animations: {
            self.bgImage.alpha = 0.6
        }) { (true) in
            self.showTitle()
        }
    }
    func showTitle() {
        UIView.animate(withDuration: 0.5, animations: {
            self.titleLabel.alpha = 1
        }) { (true) in
            self.showDescLabel()
        }
    }
    func showDescLabel() {
        UIView.animate(withDuration: 0.5, animations: {
            self.descLabel.alpha = 1
        }) { (true) in
            self.showButtonAndText()
        }
    }
    func showButtonAndText() {
        UIView.animate(withDuration: 0.5) {
            self.playButton.alpha = 1
            self.findLabel.alpha = 1
        }
    }


}

