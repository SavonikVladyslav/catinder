//
//  PhotoResponse.swift
//  caTinder
//
//  Created by MACsimus on 12.05.2020.
//  Copyright © 2020 VladyslavSavonik. All rights reserved.
//

import Foundation

struct PhotoResponse: Codable {
    
    let width: Int
    let height: Int
    
    let id: String
    let url: String
    
    
}
